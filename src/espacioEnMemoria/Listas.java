/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package espacioEnMemoria;
import org.github.jamm.MemoryMeter;
import java.util.ArrayList;
import java.util.List;



/**
 *
 * @author diego
 */
public class Listas {
    public static void main(String[] args) {
        MemoryMeter memoria = MemoryMeter.builder().build();
        List<Integer> numeros;
        List<Integer> numeros1;
        
        numeros = new ArrayList<>();
        
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Lista vacía:" + memoria.measureDeep(numeros)+" bits");
        
        System.out.println("---------------------------");
        System.out.println("Elementos lista llena:");
        numeros1 = new ArrayList<>();
        int cont=0;
        for (int i = 0; i < 25; i++) {
            cont += 1;
            numeros1.add(cont);
            System.out.println(">"+cont);
            
        }
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Lista llena:" + memoria.measureDeep(numeros1)+" bits");
        System.out.println("------------------------------------------");
    }
    
    
        
    
    
}
