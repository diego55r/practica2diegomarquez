/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package espacioEnMemoria;

import org.github.jamm.MemoryMeter;

/**
 *
 * @author diego
 */
public class Arreglos {

    public static void main(String[] args) {
        MemoryMeter memoria = MemoryMeter.builder().build();

//String vacío
        String[] arreglo_string = new String[0];
        System.out.println("------------------------------------------");
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> String vacío: "+memoria.measureDeep(arreglo_string));
//String LLeno        
        String[] arreglo_string1 = {"hola","mundo"};
        System.out.println("> String lleno:" +memoria.measureDeep(arreglo_string1));
        System.out.println("------------------------------------------");
        
        
//Int vacío
        int[] arreglo_int = new int[0];
        System.out.println("------------------------------------------");
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Int vacío: "+memoria.measureDeep(arreglo_int));
//Int LLeno        
        int[] arreglo_int1 = {1,2,3};
        System.out.println("> Int lleno:" +memoria.measureDeep(arreglo_int1));
        System.out.println("------------------------------------------");
        
        
//char vacío
        char[] arreglo_char = new char[0];
        System.out.println("------------------------------------------");
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Char vacío: "+memoria.measureDeep(arreglo_char));
//char LLeno        
        char[] arreglo_char1 = {'a','b'};
        System.out.println("> Char lleno:" +memoria.measureDeep(arreglo_char1));
        System.out.println("------------------------------------------");
        
        
//short vacío
        short[] arreglo_short = new short[0];
        System.out.println("------------------------------------------");
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Short vacío: "+memoria.measureDeep(arreglo_short));
//short LLeno        
        short[] arreglo_short1 = {12345,6523,5698};
        System.out.println("> Short lleno:" +memoria.measureDeep(arreglo_short1));
        System.out.println("------------------------------------------");
        
        
//boolean vacío
        boolean[] arreglo_boolean = new boolean[0];
        System.out.println("------------------------------------------");
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Boolean vacío: "+memoria.measureDeep(arreglo_boolean));
//boolean LLeno        
        boolean[] arreglo_boolean1 = {false,true,true,false,false};
        System.out.println("> Boolean lleno:" +memoria.measureDeep(arreglo_boolean1));
        System.out.println("------------------------------------------");
        

    }
}

