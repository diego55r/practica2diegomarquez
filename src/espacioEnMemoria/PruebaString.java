/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package espacioEnMemoria;

import org.github.jamm.MemoryMeter;

/**
 *
 * @author diego
 */
public class PruebaString {

    public static void main(String[] args) {
        
        MemoryMeter memoria = MemoryMeter.builder().build();
        //Se crea un String vacío
        String vacio = "";
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println(memoria.measureDeep(vacio));
        
        //Comparamos con String completos
        String lleno = "hola mundo mundial";
        System.out.println(memoria.measureDeep(lleno));
        
        //Comparamos con String largo
        String verso = "El pájaro enjaulado canta un temeroso trino sobre algo desconocido mas ansiado aún y desde la lejana colina se escucha la melodía pues el pájaro enjaulado canta a la libertad.";
        System.out.println(memoria.measureDeep(verso));
        
    }

}
