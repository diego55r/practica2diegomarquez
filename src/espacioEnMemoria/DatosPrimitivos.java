/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package espacioEnMemoria;

import org.github.jamm.MemoryMeter;

/**
 *
 * @author diego
 */
public class DatosPrimitivos {

    public static void main(String[] args) {

        MemoryMeter memoria = MemoryMeter.builder().build();
        //TIPOS DE DATOS PRIMITIVOS

//Byte
        byte dato_byte = 0;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("------------------------------------------");
        System.out.println("> Byte vacío:" + memoria.measureDeep(dato_byte));
        byte dato_byte1 = 127;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Byte lleno:" + memoria.measureDeep(dato_byte1));
        System.out.println("------------------------------------------");

//Int
        int dato_int = 0;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Int vacío:" + memoria.measureDeep(dato_int));
        int dato_int1 = 921659856;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Int lleno:" + memoria.measureDeep(dato_int1));
        System.out.println("------------------------------------------");

        
//Long
        long dato_long = 0;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Long vacío:" + memoria.measureDeep(dato_long));
        long dato_long1 = 1276516565;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Long lleno:" + memoria.measureDeep(dato_long1));
        System.out.println("------------------------------------------");

        
//float
        float dato_float = 0;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Float vacío:" + memoria.measureDeep(dato_float));
        float dato_float1 = -20;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Float lleno:" + memoria.measureDeep(dato_float1));
        System.out.println("------------------------------------------");

        
//Short
        short dato_short = 0;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Short vacío:" + memoria.measureDeep(dato_short));
        short dato_short1 = 12765;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Short lleno:" + memoria.measureDeep(dato_short1));
        System.out.println("------------------------------------------");

        
//char
        char dato_char = 0;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Char vacío:" + memoria.measureDeep(dato_char));
        char dato_char1 = 'a';
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Char lleno:" + memoria.measureDeep(dato_char1));
        System.out.println("------------------------------------------");

        
//double
        double dato_double = 0;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Double vacío:" + memoria.measureDeep(dato_double));
        double dato_double1 = 127.50;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Double lleno:" + memoria.measureDeep(dato_double1));
        System.out.println("------------------------------------------");

        
//boolean
        boolean dato_boolean = false;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Boolean vacío:" + memoria.measureDeep(dato_boolean));
        boolean dato_boolean1 = true;
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Boolean lleno:" + memoria.measureDeep(dato_boolean1));
        System.out.println("------------------------------------------");

    }

}

/* NOTA: Podemos concluir que no hay valores vacíos para las variables primitivas, 
         y dado esto no hay una forma de comparación de "vacíos"
*/
