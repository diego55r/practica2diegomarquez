/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package listaEnlazada;

import org.github.jamm.MemoryMeter;

/**
 *
 * @author diego
 */
public class Main {
    public static void main(String[] args) {
        MemoryMeter memoria = MemoryMeter.builder().build();
        
        //Creamos ListaEnlazada vacía
        ListaEnlazada listaen = new ListaEnlazada();
        
        
        System.out.println(">Lista enlazada vacía: " + listaen.estaVacia());
        System.out.println(memoria.measureDeep(listaen)+"bits");
        
        //Creamos lista enlazada con 25 elementos
        System.out.println("---------------------------");
        System.out.println("Elementos lista llena:");
        listaen = new ListaEnlazada();
        int cont=0;
        for (int i = 0; i < 25; i++) {
            cont += 1;
            listaen.addPrimero(cont);
            System.out.println(">"+cont);
            
        }
        //measureDeep = nos indica cuanto ocupa un objeto o array determinado.
        System.out.println("> Lista llena:" + memoria.measureDeep(listaen)+" bits");
        System.out.println("------------------------------------------");
    }
}
