/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package listaEnlazada;
import org.github.jamm.MemoryMeter;
/**
 *
 * @author diego
 */
public class ListaEnlazada {
    
     Nodo cabecera;
     int size;
     
     public ListaEnlazada(){
         cabecera = null;
         size =0;
     }
     
     public Object obtener(int pos){
         
         int cont=0;
         
         Nodo temporal = cabecera;
         while (cont < pos) {
             temporal = temporal.obtenerSiguiente();
             cont++;
             
         }
         return temporal.obtenerValor();
     }
     public void addPrimero(Object obj){
         if (cabecera == null) {
             
             cabecera = new Nodo(obj);
         }else{
             Nodo temp = cabecera;
             Nodo nuevo = new Nodo(obj);
             nuevo.enlazarSiguiente(temp);
             cabecera = nuevo;
         }
         size++;
     }
     
     public int size(){
         return size;
     }
     
     public boolean estaVacia(){
         return(cabecera == null)?true:false;
     }
}
